# Machine Learning: introduction and end to end project with Scikit-Learn

- **Description:** This module introduces Machine Learning and goes through an end to end project. The two notebooks cover respectively Chapter 1 and 2 of Aurélien Géron's very resourceful second edition of his book [Hands-on Machine Learning with Scikit-Learn, Keras and TensorFlow](https://www.oreilly.com/library/view/hands-on-machine-learning/9781492032632/):

<img src="https://images-na.ssl-images-amazon.com/images/I/51aqYc1QyrL._SX379_BO1,204,203,200_.jpg" title="book" width="250" />

- **Copy-right:**
  - Hence the two notebooks I created are not being made public for copy-right reasons with regards to Aurélien Géron's work. You can however consult the original notebook he created: [02_end_to_end_machine_learning_project.ipynb](https://github.com/ageron/handson-ml2/blob/master/02_end_to_end_machine_learning_project.ipynb).
  - If you want to learn more about how to test and validate your data, you can download the pdf [testing_and_validating_your_data.pdf](./img/testing_and_validating_your_data.pdf), which I created as a complement to Aurélien Géron's explanations.

- **Module's objectives:**
  - Introduce Machine Learning concepts, vocabulary (first notebook, Chapter 1 of the book).
  - Work through an example project end to end (second notebook, Chapter 2 of the book).
    1. Look at the big picture.
    2. Get the data.
    3. Discover and visualize the data to gain insights.
    4. Prepare the data for Machine Learning algorithms.
    5. Select a model and train it.
    6. Fine-tune your model.
    7. Present your solution.
    8. Launch, monitor, and maintain your system.
- **Estimated duration:** ~6h
