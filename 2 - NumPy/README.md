# NumPy

- **Description:** la 1er notebook présente NumPy et les exercices sont tous contenu dans le second notebook.
- **Objectifs du module:**
  - Propriétés du `ndarray`, la structure de base `numpy`.
  - Créer facilement des `ndarray`.
  - Ré-agencer la forme des `ndarray` pour les manipuler.
  - Pourquoi éviter les boucles `for` en Python.
  - Lire et écrire dans un fichier.
- **Temps estimé:** 3h.
