# Créer son projet en Python

[Contenu du module à créer !]

- **Description:** ...
- **Objectifs du module:**
  - Les environnements virtuel `conda`.
  - Utiliser un IDE: Visual Studio Code ou PyCharm.
  - Mon premier script `.py`.
  - Structure classique d'un projet Python.
  - Créer un module et l'importer dans un autre script.
  - Les bonnes pratiques de design: PEP8.
  - Créer sa documentation.
  - Tests unitaires automatiques.
  - Comment packager son code.
- **Temps estimé:** ...


### To do
- fichier markdown pour expliquer les env virtuels conda, commandes principales etc
- fichier markdown / vidéo pour montrer VS Code ou PyCharm ?
- Créer un ensemble de dossier, sous-dossier type avec des fichiers pré-rempli genre README.md, environment.yml, .gitignore, /utils, /core, /plots, main.py etc
- Faire un markdown avec des instructions de création de dossiers, sous-dossier, script et fonctions à créer pour avoir en fin d'exercice un module plus abouti.
- Markdown pour apprendre à:
  - Créer sa doc
  - Faire des test unitaires
  - Packager son code
