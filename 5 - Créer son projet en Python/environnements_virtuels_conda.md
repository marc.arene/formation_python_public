# Environnements virtuels `conda`

- [Documentation officielle de `conda`](https://docs.conda.io/projects/conda/en/latest/user-guide/index.html)
- Description:
  - "Conda is an open-source package management system and environment management system that runs on Windows, macOS, and Linux. Conda quickly installs, runs, and updates packages and their dependencies. Conda easily creates, saves, loads, and switches between environments on your local computer. It was created for Python programs but it can package and distribute software for any language."
  - Conda est automatiquement installé lorsqu'on installe la distribution Anaconda.
  - Lors du démarrage d'un projet, l'idée est de créer un environnement virtuel conda spécifique au projet. En son sein seront installées la version voulue de Python ainsi que les versions de chacune des librairies nécessaires au projet. Ainsi cela nous permet de cloisonner les projets pour éviter tout conflit de version, cela permet également de faciliter l'installation des librairies du projet pars des tiers.


## Commandes principales

Sur Windows, les commandes doivent être exécutées depuis l'invite __Anaconda Prompt__, sur macOS et Linux le terminal suffit.

Vous pouvez consulter la liste exhaustive des commandes [ici](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#).

### Créer un environnement virtuel
Prenons l'exemple de l'environnement virtuel `gutipy_formation` que vous avez dû créer pour cette formation.
```
(base) C:\Users\Marc Arène>conda create --name gutipy_formation
```

### Activer et désactiver un environnement
```
(base) C:\Users\Marc Arène>conda activate gutipy_formation

(gutipy_formation) C:\Users\Marc Arène>conda deactivate

(base) C:\Users\Marc Arène>
```
Le nom de l'environnement actif apparaît entre parenthèses en début de ligne.

### Installer une librairie
```
(gutipy_formation) C:\Users\Marc Arène>conda install numpy
```
Note: il existe d'autres gestionnaires de librairies (e.g. `pip`, `brew`) dont vous entendrez souvent parler et que différentes pages du web vous recommanderont d'utiliser pour l'installation de tel ou tel package. Si vous utilisez `conda`, privilégiez toujours l'installation avec la commande `conda install`. Si cela ne marche pas, essayez dans un second temps avec `pip` qui fonctionne bien avec `conda`; pour les autres vous risquez de vous heurter à des conflits d'installation.

### Cloner un environnement
```
(base) C:\Users\Marc Arène>conda create --name gutipy_formation_cloned --clone gutipy_formation
```

### Créer un environnement avec une version spécifique de Python et d'autres packages
```
(base) C:\Users\Marc Arène>conda create -n gutipy_formation python=3.6 scipy=0.15.0 pandas
```

### Créer un environnement à partir d'un fichier `.yml`
Ces fichiers, généralement nommés `environment.yml`, regroupent toutes les infos nécessaires à la création d'un environnement informatique dont dépend le projet. Vous pouvez par exemple consulter le [environment.yml](../environment.yml) de ce repo.
```
(base) C:\Users\Marc Arène>conda env create -f environment.yml
```

### Afficher la liste des environnements de ma machine
```
(base) C:\Users\Marc Arène>conda env list
# conda environments:
#
base                  *  C:\Users\Public\anaconda3
gutipy_formation         C:\Users\Public\anaconda3\envs\gutipy_formation
```
Le chemin d'installation est spécifié pour chaque environnement; à part le `base`, ils sont tous créés dans `anaconda3\envs\` et les fichiers des librairies qu'ils contiennent seront installées dans leur sous-répertoire propre.

### Supprimer un environnement
```
(base) C:\Users\Marc Arène>conda remove --name gutipy_formation --all
```
