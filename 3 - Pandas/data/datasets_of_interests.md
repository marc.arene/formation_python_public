# Some datasets of interests on Kaggle

- [French employment, salaries, population per town](https://www.kaggle.com/etiennelq/french-employment-by-town?select=name_geographic_information.csv)
- [International Energy Statistics](https://www.kaggle.com/unitednations/international-energy-statistics)
- [2021 Olympics in Tokyo](https://www.kaggle.com/arjunprasadsarkhel/2021-olympics-in-tokyo)
- [Countries population](https://www.kaggle.com/cityapiio/countries-population-2010-2020-data)
- [Fifa 19 players](https://www.kaggle.com/karangadiya/fifa19)
