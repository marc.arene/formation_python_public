# Pandas

- **Description:** Module complet sur la librairie d'analyse de données `pandas`, de l'introduction à une utilisation avancée des outils proposés.
- **Objectifs du module:**
  - Comprendre les deux structures de base de `pandas`: `DataFrame` et `Series`.
  - Inspecter rapidement un jeu de données, numériquement et visuellement.
  - Manipuler `DataFrame` en filtrant lignes et colonnes.
  - Agréger les données et faire des analyses poussées.
  - Nettoyer et transformer un jeu de données pour pouvoir mieux le manipuler (2e notebook).
- **Temps estimé:** 4h.

- Autres tuto:
  - https://www.kaggle.com/sohier/tutorial-accessing-data-with-pandas
  - https://www.kaggle.com/kralmachine/pandas-tutorial-for-beginners
  - https://www.youtube.com/watch?v=vmEHCJofslg
