# Formation Python - Data Science

- Auteur: _Marc Arène_
- Contact: _arene.marc@gmail.com_

## Description générale
Ce repository contient le matériel pour suivre une formation Python orientée Data Science, qui s'adresse aussi bien à des débutants en Python qu'à des profils plus expérimentés.

Chaque module de la formation est dans un dossier de ce repo et nous conseillons de les suivres dans l'ordre indiqué, surtout si vous débutez en Python.

## Prérequis d'installation
Pour pouvoir suivre cette formation, il est nécessaire d'avoir les bonnes librairies Python et Jupyter installées: il vous faut donc suivre les [**prérequis d'installation**](./prerequis_installation.md).

## Description des modules
**1. Premiers pas en Python** [~3h]
  - Présentation générale et découverte de la syntaxe.
  - Manipuler les principaux types de variables: `str`, `list` et `dict`.
  - Boucle `for`, `if... elif... else`.
  - Définir une fonction.
  - Définir une classe.
  - Lire et écrire dans un fichier.

**2. NumPy** [~3h]
  - Propriétés du `ndarray`, la structure de base `numpy`.
  - Créer facilement des `ndarray`.
  - Ré-agencer la forme des `ndarray` pour les manipuler.
  - Pourquoi éviter les boucles `for` en Python.
  - Lire et écrire dans un fichier.

**3. Pandas** [~4h]
  - Comprendre les deux structures de base de `pandas`: `DataFrame` et `Series`.
  - Inspecter rapidement un jeu de données, numériquement et visuellement.
  - Manipuler `DataFrame` en filtrant lignes et colonnes.
  - Agréger les données et faire des analyses poussées.
  - Nettoyer et transformer un jeu de données pour pouvoir mieux le manipuler (2e notebook).

**4. Visualiser les données avec Matplotlib et Plotly** [~3h]
  - Créer les principaux types de graphiques avec `matplotlib` (line, bar, histogram et scatter plots)
  - Créer les principaux types de graphiques avec `plotly` (line, bar, histogram, scatter, map scatter, animated plots)
  - Reproduire un plot avancé et comprendre les méthodes Monte Carlo.

**5. Créer son projet en Python**
  - Les environnements virtuel `conda`.
  - Utiliser un IDE: Visual Studio Code ou PyCharm.
  - Mon premier script `.py`.
  - Structure classique d'un projet Python.
  - Créer un module et l'importer dans un autre script.
  - Les bonnes pratiques de design: PEP8.
  - Créer sa documentation
  - Tests unitaires automatiques.

**6. Machine Learning - introduction et project complet avec Scikit-Learn** [~6h]
  - Introduce Machine Learning concepts, vocabulary.
  - Work through an example project end to end.
    1. Look at the big picture.
    2. Get the data.
    3. Discover and visualize the data to gain insights.
    4. Prepare the data for Machine Learning algorithms.
    5. Select a model and train it.
    6. Fine-tune your model.
    7. Present your solution.
    8. Launch, monitor, and maintain your system.

**7. Deep Learning avec Keras et Tensorflow** [~6h]
  - Introduce Deep Learning concepts (first notebook).
  - Work through an example project (second notebook, Chapter 10 of the book).
    1. Building an image classifier
    2. Building a Regression Multi-Layer Perceptron
    3. TensorBoard
    4. Hyperparameter Tuning
