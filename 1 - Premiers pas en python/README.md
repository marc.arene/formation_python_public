# Premiers pas en Python

- **Description:** la présentation et les exercices sont contenus dans le seul notebook de ce dossier.
- **Objectifs du module:**
  - Présentation générale et découverte de la syntaxe.
  - Manipuler les principaux types de variables: `str`, `list` et `dict`.
  - Boucle `for`, `if... elif... else`.
  - Définir une fonction.
  - Définir une classe.
  - Lire et écrire dans un fichier.
- **Temps estimé:** 3h pour un néophyte, 1h pour un développeur expérimenté avec quelques notions en Python.
