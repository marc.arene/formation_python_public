# Prérequis d'installation pour suivre la formation

Ces prérequis vous permettront de suivre la formation de manière intéractive sur votre ordinateur et de refaire les exercices ultérieurement.


## Télécharger la formation depuis ce repo

Nous vous recommandons et considèrerons par la suite que ce repo sera enregistré localement dans un sous-dossier `/projets` de votre "home directory", ie dans `C:\Users\Marc Arène\projets\` sur Windows ou bien `/Users/marcarene/projets/` sur Mac/Linux.

### Manuellement
- Tout simplement en cliquant sur le bouton "Download" à gauche du bouton bleu "Clone" à la racine du repo: https://gitlab.com/gutilab/formation_python/-/tree/master.
- Enregistrez le contenu dans un sous-dossier `/projets` de votre home et décompresser le ``.zip``.

### Ou bien avec git (recommandé si vous connaissez git)
#### Windows
- Télécharger `git-scm` [ici](https://git-scm.com/download/win) si ce vous ne l'avez pas déjà, ou bien utilisez `git` directement depuis votre IDE comme Visual Studio Code par exemple (?).
- Si vous utilisez `git-scm`, ouvrez une invite de commande et clonez le repo localement dans votre sous-dossier  `/projets`:
  ```bash
  Marc Arène@DESKTOP-C1I2NJ3 MINGW64 ~/projets
  $ git clone https://gitlab.com/gutilab/formation_python.git
  Cloning into 'formation_python'...
  info: detecting host provider for 'https://gitlab.com/'...
  info: detecting host provider for 'https://gitlab.com/'...
  remote: Enumerating objects: 49, done.
  remote: Counting objects: 100% (49/49), done.
  remote: Compressing objects: 100% (49/49), done.
  remote: Total 49 (delta 22), reused 0 (delta 0), pack-reused 0
  Receiving objects: 100% (49/49), 928.80 KiB | 4.20 MiB/s, done.
  Resolving deltas: 100% (22/22), done.
  ```

#### Mac / Linux
  Vous devriez déjà avoir git installé sur votre poste, il vous suffit donc d'aller dans votre dossier `/Users/marcarene/projets/` et de cloner le repo:
  ```bash
  marcarene in ~/projets 
  $ git clone git@gitlab.com:gutilab/formation_python.git
  Cloning into 'formation_python'...
  info: detecting host provider for 'https://gitlab.com/'...
  info: detecting host provider for 'https://gitlab.com/'...
  remote: Enumerating objects: 49, done.
  remote: Counting objects: 100% (49/49), done.
  remote: Compressing objects: 100% (49/49), done.
  remote: Total 49 (delta 22), reused 0 (delta 0), pack-reused 0
  Receiving objects: 100% (49/49), 928.80 KiB | 4.20 MiB/s, done.
  Resolving deltas: 100% (22/22), done.
  ```


## Installer Anaconda
### Windows
- [Lien de téléchargement](https://www.anaconda.com/download/#windows)
- Suivez les [instructions](https://docs.anaconda.com/anaconda/install/windows/), en faisant attention notamment au point suivant:
  - _"Destination Folder should not contain an empty space or Unicode characters outside the 7-bit ASCII character set"_. Si, comme moi, votre nom d'utilisateur contient un espace ou bien un accent: `C:\Users\Marc Arène`, je vous recommande donc de choisir le chemin suivant: `C:\Users\Public\anaconda3`.

### Mac / Linux
- [Lien de téléchargement](https://docs.anaconda.com/anaconda/install/mac-os/)




## Créer l'environnement virtuel de la formation
### Windows
 - Ouvrez un terminal Anaconda Prompt depuis le menu Démarrer.
 - Allez dans le dossier de la formation que vous avez préalablement téléchargée:
 ```
 (base) C:\Users\Marc Arène>cd projets\formation_python
 ```
 - Créez l'environnement virtuel conda spécifique à la formation à partir du fichier `environment.yml` présent à la racine de ce repo:
  ```
  (base) C:\Users\Marc Arène\projets\formation_python>conda env create -f environment.yml
  ```
 Attendez que toutes les librairies se téléchargent ce qui peut prendre plusieurs minutes. Vous venez de créer un environnement qui se nomme `gutipy_formation` et qui contient les librairies requises pour suivre la formation.
- Activez cet environnment et vérifiez que son nom apparaît en préambule de la ligne de commande:
  ```
  (base) C:\Users\Marc Arène\projets\formation_python>conda activate gutipy_formation
  (gutipy_formation) C:\Users\Marc Arène\projets\formation_python>
  ```

### Mac / Linux
 - Ouvrez un terminal et allez dans le dossier de la formation que vous avez préalablement téléchargée:
  ```bash
  (base) marcarene in ~
  $ cd projets/formation_python
  ```
 - Créez l'environnement virtuel conda spécifique à la formation à partir du fichier `environment.yml` présent à la racine de ce repo:
  ```bash
  (base) marcarene in ~/projets/formation_python
  $ conda env create -f environment.yml
  ```
  Attendez que toutes les librairies se téléchargent ce qui peut prendre plusieurs minutes. Vous venez de créer un environnement qui se nomme `gutipy_formation` et qui contient les librairies requises pour suivre la formation.
- Activez cet environnment et vérifiez que son nom apparaît en préambule de la ligne de commande:
  ```bash
  (base) marcarene in ~/projets/formation_python
  $ conda activate gutipy_formation
  (gutipy_formation) marcarene in ~/projets/formation_python 
  $
  ```




## Lancer Jupyter Notebook
### Windows
- Dans l'environnement conda `gutipy_formation` activé, finissez l'installation de la librairie `jupyter_contrib_nbextensions` avec la commande suivante:
  ```
  (gutipy_formation) C:\Users\Marc Arène\projets\formation_python>jupyter contrib nbextension install --sys-prefix
  ```
- Lancez Jupyter Notebook en ligne de commande depuis le dossier parent de la formation:
  ```
  (gutipy_formation) C:\Users\Marc Arène\projets\formation_python>jupyter-notebook
  [I 18:28:08.259 NotebookApp] Writing notebook server cookie secret to C:\Users\Marc Arène\AppData\Roaming\jupyter\runtime\notebook_cookie_secret
  [I 18:28:08.845 NotebookApp] [jupyter_nbextensions_configurator] enabled 0.4.1
  [I 18:28:09.099 NotebookApp] JupyterLab extension loaded from C:\Users\Public\anaconda3\envs\gutipy_formation\lib\site-packages\jupyterlab
  [I 18:28:09.099 NotebookApp] JupyterLab application directory is C:\Users\Public\anaconda3\envs\gutipy_formation\share\jupyter\lab
  [I 18:28:09.099 NotebookApp] Serving notebooks from local directory: C:\Users\Marc Arène\projets\formation_python
  [I 18:28:09.099 NotebookApp] Jupyter Notebook 6.1.4 is running at:
  [I 18:28:09.115 NotebookApp] http://localhost:8888/?token=14b8e1bd13cc130b4a323f987b7bff857b54af91e6c3dff4
  [I 18:28:09.115 NotebookApp]  or http://127.0.0.1:8888/?token=14b8e1bd13cc130b4a323f987b7bff857b54af91e6c3dff4
  [I 18:28:09.115 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
  [C 18:28:09.177 NotebookApp]

      To access the notebook, open this file in a browser:
          file:///C:/Users/Marc%20Ar%C3%A8ne/AppData/Roaming/jupyter/runtime/nbserver-14276-open.html
      Or copy and paste one of these URLs:
          http://localhost:8888/?token=14b8e1bd13cc130b4a323f987b7bff857b54af91e6c3dff4
      or http://127.0.0.1:8888/?token=14b8e1bd13cc130b4a323f987b7bff857b54af91e6c3dff4
  [I 18:28:46.720 NotebookApp] 302 GET /?token=14b8e1bd13cc130b4a323f987b7bff857b54af91e6c3dff4 (127.0.0.1) 0.00ms
  ```
- Copier-collez l'URL qui vous est donnée, ici: `http://localhost:8888/?token=14b8e1bd13cc130b4a323f987b7bff857b54af91e6c3dff4`, dans votre navigateur web préféré.
- Ne fermez pas cette invite de commande tant que vous travaillez sur le notebook ou bien cela quittera l'application!


### Mac / Linux
- Dans l'environnement conda `gutipy_formation` activé lancez la commande suivante depuis le sous-dossier `~/projets/formation_python`
  ```bash
  (gutipy_formation) marcarene in ~/projets/formation_python
  $ jupyter-notebook
  ```
- Jupyter Notebook devrait s'ouvrir automatiquement dans votre navigateur web.
- Ne fermez pas cette invite de commande tant que vous travaillez sur le notebook ou bien cela quittera l'application!








## Paramétrer Jupyter Notebook
L'environnement conda `gutipy_formation` contient des extensions non standards de l'application Jupyter Notebook qui vont nous être utiles et que nous devons activer.

- Pour cela, une fois le dossier de la formation ouvert dans Jupyter Notebook, allez dans l'onglet `NBExtensions`.
- Décochez la case indiquant _"disable configuration for nbextensions without explicit compatibility (they may break your notebook environment, but can be useful to show for nbextension development)"_ (cf image ci-dessous).
- Puis activez les deux extensions suivantes: _"Exercise2"_ et _"Table of Content (2)"_ ([capture d'écran ici si l'image en dessous ne s'affiche pas](https://gitlab.com/gutilab/formation_python/-/blob/master/_temp/images/Capture_nbext_2.PNG)).

![https://gitlab.com/gutilab/formation_python/-/blob/master/_temp/images/Capture_nbext_2.PNG](./_temp/images/Capture_nbext_2.png)
