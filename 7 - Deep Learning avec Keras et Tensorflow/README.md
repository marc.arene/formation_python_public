# Deep Learning with Keras and Tensorflow

- **Description:** This module introduces Deep Learning both theoretically and practically. The first notebook simply links resourceful youtube videos which explain brilliantly the ins and outs of Deep Learning. The second notebook covers most of Chapter 10 of Aurélien Géron's very resourceful second edition of his book [Hands-on Machine Learning with Scikit-Learn, Keras and TensorFlow](https://www.oreilly.com/library/view/hands-on-machine-learning/9781492032632/):

<img src="https://images-na.ssl-images-amazon.com/images/I/51aqYc1QyrL._SX379_BO1,204,203,200_.jpg" title="book" width="250" />

- **Copy-right:** Hence the second notebook I created is not being made public for copy-right reasons with regards to Aurélien Géron's work. You can however consult the original notebook he created: [10_neural_nets_with_keras.ipynb.ipynb](https://github.com/ageron/handson-ml2/blob/master/10_neural_nets_with_keras.ipynb) or download directly the copy here.

- **Module's objectives:**
  - Introduce Deep Learning concepts (first notebook).
  - Work through an example project (second notebook, Chapter 10 of the book).
    1. Building an image classifier
    2. Building a Regression Multi-Layer Perceptron
    3. TensorBoard
    4. Hyperparameter Tuning
- **Estimated duration:** ~6h
