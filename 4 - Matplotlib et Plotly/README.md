# Visualisation des données avec Matplotlib et Plotly

- **Description:** Manipuler les bibliothèques `matplotlib` et `plotly`, comprendre leur philosophie, pour être capable de créer les principaux types de graphiques (aussi nommés "plots") et les personnaliser. Le notebook `3_monte_carlo_methods.ipynb` introduit les méthodes Monte Carlo tout en s'exerçant à la reproduction d'un graphique avancé.
- **Objectifs du module:**
  - Créer les principaux types de graphiques avec `matplotlib` (line, bar, histogram et scatter plots)
  - Créer les principaux types de graphiques avec `plotly` (line, bar, histogram, scatter, map scatter, animated plots)
  - Reproduire un plot avancé et comprendre les méthodes Monte Carlo.
- **Temps estimé:** 3h.
