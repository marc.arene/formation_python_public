{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Monte Carlo methods\n",
    "\n",
    "- **Auteur**: Marc Arène\n",
    "- **Contact**: marc.arene@gutilab.com\n",
    "- **Temps estimé**: 1h\n",
    "\n",
    "Text and content freely adapted from the PhD thesis: [DeepHMC: a deep learning Hamiltonian Monte Carlo algorithm for Bayesian inference of compact binary sources of gravitational waves](http://www.theses.fr/s188928), defended in 2021 by Marc Arène."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#History-and-definition\" data-toc-modified-id=\"History-and-definition-1\"><span class=\"toc-item-num\">1&nbsp;&nbsp;</span>History and definition</a></span></li><li><span><a href=\"#Illustration-with-an-estimation-of-$\\pi$\" data-toc-modified-id=\"Illustration-with-an-estimation-of-$\\pi$-2\"><span class=\"toc-item-num\">2&nbsp;&nbsp;</span>Illustration with an estimation of $\\pi$</a></span></li><li><span><a href=\"#Reproducing-the-left-panel-plot\" data-toc-modified-id=\"Reproducing-the-left-panel-plot-3\"><span class=\"toc-item-num\">3&nbsp;&nbsp;</span>Reproducing the left panel plot</a></span><ul class=\"toc-item\"><li><span><a href=\"#Generate-the-population-of-400-hundred-points\" data-toc-modified-id=\"Generate-the-population-of-400-hundred-points-3.1\"><span class=\"toc-item-num\">3.1&nbsp;&nbsp;</span>Generate the population of 400 hundred points</a></span></li><li><span><a href=\"#Create-the-scatter-plot-from-the-left-panel\" data-toc-modified-id=\"Create-the-scatter-plot-from-the-left-panel-3.2\"><span class=\"toc-item-num\">3.2&nbsp;&nbsp;</span>Create the scatter plot from the left panel</a></span><ul class=\"toc-item\"><li><span><a href=\"#Plot-the-400-points-on-a-scatter-plot\" data-toc-modified-id=\"Plot-the-400-points-on-a-scatter-plot-3.2.1\"><span class=\"toc-item-num\">3.2.1&nbsp;&nbsp;</span>Plot the 400 points on a scatter plot</a></span></li><li><span><a href=\"#Add-the-circle-and-the-square\" data-toc-modified-id=\"Add-the-circle-and-the-square-3.2.2\"><span class=\"toc-item-num\">3.2.2&nbsp;&nbsp;</span>Add the circle and the square</a></span></li><li><span><a href=\"#Customize-your-plot\" data-toc-modified-id=\"Customize-your-plot-3.2.3\"><span class=\"toc-item-num\">3.2.3&nbsp;&nbsp;</span>Customize your plot</a></span></li></ul></li></ul></li><li><span><a href=\"#Reproducing-the-right-panel-plot\" data-toc-modified-id=\"Reproducing-the-right-panel-plot-4\"><span class=\"toc-item-num\">4&nbsp;&nbsp;</span>Reproducing the right panel plot</a></span><ul class=\"toc-item\"><li><span><a href=\"#Generate-a-series-of-$\\pi$-estimates\" data-toc-modified-id=\"Generate-a-series-of-$\\pi$-estimates-4.1\"><span class=\"toc-item-num\">4.1&nbsp;&nbsp;</span>Generate a series of $\\pi$ estimates</a></span></li><li><span><a href=\"#Plot-the-black-curve:-$\\hat{\\pi}(N)$\" data-toc-modified-id=\"Plot-the-black-curve:-$\\hat{\\pi}(N)$-4.2\"><span class=\"toc-item-num\">4.2&nbsp;&nbsp;</span>Plot the black curve: $\\hat{\\pi}(N)$</a></span></li><li><span><a href=\"#Plot-the-blue-and-red-curves\" data-toc-modified-id=\"Plot-the-blue-and-red-curves-4.3\"><span class=\"toc-item-num\">4.3&nbsp;&nbsp;</span>Plot the blue and red curves</a></span></li></ul></li><li><span><a href=\"#Combine-the-two-previous-panel-plots-on-a-single-figure\" data-toc-modified-id=\"Combine-the-two-previous-panel-plots-on-a-single-figure-5\"><span class=\"toc-item-num\">5&nbsp;&nbsp;</span>Combine the two previous panel plots on a single figure</a></span></li><li><span><a href=\"#Ultimate-plot-customization\" data-toc-modified-id=\"Ultimate-plot-customization-6\"><span class=\"toc-item-num\">6&nbsp;&nbsp;</span>Ultimate plot customization</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## History and definition\n",
    "\n",
    "Monte Carlo methods comprise a wide class of techniques and algorithms aimed at solving problems with the help of random (i.e. stochastic) processes.  A major application of these techniques is in the estimation of multidimensional integrals, especially those with irregular boundary conditions.\n",
    "\n",
    "The start of this field can be dated back as far as the $\\text{XIX}^{\\text{th}}$ century where Laplace suggested in 1812 to approximate $\\pi$ using an experiment proposed by the Comte de Buffon in 1733 where needles of length $l$ are randomly thrown on a floor stripped with parallel straight lines of equal separation $\\delta$: the expected proportion of needles crossing a line can be shown to equal $2l/(\\pi\\delta)$, thus allowing in turn an empirical estimation of $\\pi$ itself by setting $(l, \\delta)$ and throwing a given number of needles.\n",
    "\n",
    "But it is only around the year 1944 that the name Monte Carlo, referring to hazard games in casinos, and the systematic development of these methods took place [1]. The literature surrounding this field is now vast and we refer the reader to [1] for an exhaustive development on the topic. Let us yet quote a formal definition of Monte Carlo methods formulated by J. H. Halton in 1970 (cf [2], chapter 2); he defines them as\n",
    "\n",
    "\n",
    "_\"representing the solution of a problem as a parameter of a hypothetical population, and, using a random sequence of numbers to construct a sample of the population, from which statistical estimates of the parameter can be obtained.\"_\n",
    "\n",
    "[1] - [J. Hammersley, Monte Carlo Methods. Springer Science & Business Media, 2013.](https://books.google.fr/books?id=3rDvCAAAQBAJ)\n",
    "\n",
    "[2] - [I. A. Cosma and L. Evers, “Markov Chains and Monte Carlo Methods”.](https://isn.ucsd.edu/courses/beng260/complab/week2/Cosma_Evers_2010.pdf)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Illustration with an estimation of $\\pi$\n",
    "\n",
    "To illustrate the power of Monte Carlo methods, we investigate a way of empirically estimating the area of a circle, which in turns provides a numerical estimator for $\\pi$. As represented on the left panel of the figure bellow we draw a circle of generic radius $r$ embedded in a square of side length $2r$.\n",
    "  \n",
    "The ratio of the area of the circle, $\\mathcal{A}_c=\\pi r^2$, with that of the square, $\\mathcal{A}_s= (2r)^2$, is thus $\\rho=\\pi/4$. Using random number generators we generate $N=400$ independent samples uniformly drawn inside the square.\n",
    "  At every draw, the probability for a sample $X$, of coordinates $(x,y)$, to fall inside the circle is given by\n",
    "  \\begin{equation}\n",
    "    P(X\\in\\mathcal{A}_c) = \\frac{1}{\\mathcal{A}_s} \\int_{\\mathcal{A}_c} dx dy = \\frac{\\mathcal{A}_c}{\\mathcal{A}_s} = \\rho.\n",
    "  \\end{equation}\n",
    "\n",
    "  It follows that the number $N_c$ of points falling inside the circle is proportional to the area of the circle, $\\mathcal{A}_c$. Therefore $N_c/N$ is an approximation of $\\rho=\\pi/4$ and the statistical estimate $\\hat{\\pi}$ reads\n",
    "  \\begin{equation}\n",
    "    \\hat{\\pi}(N) = 4 \\frac{N_c}{N} \\xrightarrow[N\\to\\infty]{}\\pi,\n",
    "  \\end{equation}\n",
    "  where convergence is assured by the law of large numbers, and the speed of convergence, $\\sqrt{N}$, given by the central limit theorem.\n",
    "  Our Monte Carlo simulation finds $N_c=318$ directly leading to $\\hat{\\pi}(400) = 3.18$ when we know that $\\pi\\approx3.141592$. On the right panel of the figure we show the convergence of the procedure as $N$ increases.\n",
    "  \n",
    "![monte_carlo_example](./monte_carlo_py.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reproducing the left panel plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.ticker import MaxNLocator\n",
    "from matplotlib import rcParams\n",
    "rcParams[\"text.usetex\"] = False\n",
    "rcParams[\"font.serif\"] = \"Computer Modern Roman\"\n",
    "rcParams[\"font.family\"] = \"serif\"\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generate the population of 400 hundred points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Use `np.random.uniform()` to generate a random array of shape (400, 2) with coordinates between [-1, 1].\n",
    "2. Create two sub-groups of these points: those inside the circle (which has radius 1), and those outside.\n",
    "3. By counting the number of points inside the circle, print your estimation of $\\pi$ given the formula above!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Type your code here !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "**Solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "np.random.seed(123)\n",
    "\n",
    "N_for_plot = 400\n",
    "pts = np.random.uniform(low=-1, high=1, size=(N_for_plot, 2))\n",
    "pts_distances = np.sqrt(pts[:,0]**2 + pts[:,1]**2)\n",
    "mask_in = pts_distances <= 1\n",
    "mask_out = pts_distances > 1\n",
    "pts_in = pts[mask_in]\n",
    "pts_out = pts[mask_out]\n",
    "\n",
    "Nc = len(pts_in)\n",
    "pi_estimate = 4 * Nc/N_for_plot\n",
    "\n",
    "print(f'Out of {N_for_plot} points, {Nc} points are inside the circle',\n",
    "      f'thus pi is estimated to {pi_estimate}',\n",
    "      f'when real value is {np.pi:.8f}...')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "pts_in_lp = pts_in.copy()\n",
    "pts_out_lp = pts_out.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create the scatter plot from the left panel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the 400 points on a scatter plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Type your code here !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "**Solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "figsize=(8, 8)\n",
    "marker_size = 20\n",
    "\n",
    "fig, ax = plt.subplots(figsize=figsize)\n",
    "ax.scatter(pts_in[:,0], pts_in[:,1], s=marker_size)\n",
    "ax.scatter(pts_out[:,0], pts_out[:,1], marker='+', s=int(marker_size*3));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Add the circle and the square"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Type your code here !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "**Solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Circle points\n",
    "theta = np.linspace(0, 2*np.pi, 100)\n",
    "xc = np.cos(theta)\n",
    "yc = np.sin(theta)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Square points\n",
    "n_pts_per_side = 99\n",
    "left_side = np.linspace([-1,-1], [-1,1], n_pts_per_side, axis=0)\n",
    "top_side = np.linspace([-1,1], [1,1], n_pts_per_side, axis=0)\n",
    "right_side = np.linspace([1,1], [1,-1], n_pts_per_side, axis=0)\n",
    "bottom_side = np.linspace([1,-1], [-1,-1], n_pts_per_side, axis=0)\n",
    "square = np.concatenate((left_side, top_side, right_side, bottom_side))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Plot\n",
    "ax.plot(xc, yc)\n",
    "ax.plot(square[:,0], square[:,1]);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Customize your plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fontsize = 20\n",
    "\n",
    "# Remove the axis\n",
    "ax.axis('off')\n",
    "\n",
    "# Add text at the bottom\n",
    "ax.text(-0.2, -1.2, rf'$N = {N_for_plot}$', fontsize=fontsize)\n",
    "\n",
    "# # Rescale the limits\n",
    "# xlim = ax.get_xlim()\n",
    "# ylim = ax.get_ylim()\n",
    "# factor = 0.912\n",
    "# ax.set_xbound([factor*lim for lim in xlim])\n",
    "# ax.set_ybound([factor*lim for lim in ylim])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reproducing the right panel plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generate a series of $\\pi$ estimates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Nmin = 10\n",
    "Nmax = 4e5\n",
    "# Nmax = 4e5\n",
    "nb_mc_sim = 100\n",
    "N_list = np.logspace(np.log10(Nmin), np.log10(Nmax), nb_mc_sim).astype(int)\n",
    "pi_est_list = []\n",
    "\n",
    "for N in N_list:\n",
    "    pts = np.random.uniform(low=-1, high=1, size=(N,2))\n",
    "    pts_distances = np.sqrt(pts[:,0]**2 + pts[:,1]**2)\n",
    "    mask_in = pts_distances <= 1\n",
    "    mask_out = pts_distances > 1\n",
    "    pts_in = pts[mask_in]\n",
    "    pts_out = pts[mask_out]\n",
    "\n",
    "    Nc = len(pts_in)\n",
    "    pi_estimate = 4 * Nc/N\n",
    "    pi_est_list.append(pi_estimate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot the black curve: $\\hat{\\pi}(N)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Type your code here !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "**Solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=figsize)\n",
    "ax.plot(N_list, pi_est_list, color='black')\n",
    "ax.set_xscale('log')\n",
    "ax.set_xlabel('Number of samples N', fontsize=fontsize)\n",
    "ax.set_ylabel(r'$\\hat{\\pi}$', fontsize=fontsize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot the blue and red curves"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Type your code here !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "**Solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "ax.plot(N_list, np.pi + 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2)\n",
    "ax.plot(N_list, np.pi - 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2, label=r'$\\pi\\pm 1/\\sqrt{N}$')\n",
    "ax.hlines(np.pi, 0, N_list[-1], colors='red', linestyles='dashed', linewidth=3)\n",
    "ax.legend(fontsize=fontsize)\n",
    "fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Combine the two previous panel plots on a single figure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Type your code here !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "**Solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Create a single figure, bigger, with two axes this time\n",
    "figsize=(30, 15)\n",
    "fontsize=40\n",
    "fig, axes = plt.subplots(1, 2, figsize=figsize)\n",
    "\n",
    "# Left plot\n",
    "marker_size = 200\n",
    "axes[0].scatter(pts_in[:,0], pts_in[:,1], s=marker_size)\n",
    "axes[0].scatter(pts_out[:,0], pts_out[:,1], marker='+', s=int(marker_size*3))\n",
    "axes[0].plot(xc, yc)\n",
    "axes[0].plot(square[:,0], square[:,1])\n",
    "axes[0].axis('off')\n",
    "axes[0].text(-0.2, -1.2, rf'$N = {N_for_plot}$', fontsize=fontsize)\n",
    "\n",
    "# Right plot\n",
    "axes[1].plot(N_list, pi_est_list, color='black')\n",
    "axes[1].set_xscale('log')\n",
    "axes[1].set_xlabel(r'Number of samples N', fontsize=fontsize)\n",
    "axes[1].set_ylabel(r'$\\hat{\\pi}$', fontsize=fontsize)\n",
    "\n",
    "axes[1].plot(N_list, np.pi + 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2)\n",
    "axes[1].plot(N_list, np.pi - 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2, label=r'$\\pi\\pm 1/\\sqrt{N}$')\n",
    "axes[1].hlines(np.pi, 0, N_list[-1], colors='red', linestyles='dashed', linewidth=3)\n",
    "axes[1].legend(fontsize=fontsize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "Something wrong ? :)\n",
    "\n",
    "`pts_in` and `pts_out` in the case of 400 points have been overwritten when we created the right hand plot. You should hence use the copies we made `pts_in_lp` and `pts_in_lp`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Create a single figure, bigger, with two axes this time\n",
    "figsize=(30, 15)\n",
    "fig, axes = plt.subplots(1, 2, figsize=figsize)\n",
    "\n",
    "# Left plot\n",
    "marker_size = 200\n",
    "axes[0].scatter(pts_in_lp[:,0], pts_in_lp[:,1], s=marker_size)\n",
    "axes[0].scatter(pts_out_lp[:,0], pts_out_lp[:,1], marker='+', s=int(marker_size*3))\n",
    "axes[0].plot(xc, yc)\n",
    "axes[0].plot(square[:,0], square[:,1])\n",
    "axes[0].axis('off')\n",
    "axes[0].text(-0.2, -1.2, rf'$N = {N_for_plot}$', fontsize=fontsize)\n",
    "\n",
    "# Right plot\n",
    "axes[1].plot(N_list, pi_est_list, color='black')\n",
    "axes[1].set_xscale('log')\n",
    "axes[1].set_xlabel('Number of samples N', fontsize=fontsize)\n",
    "axes[1].set_ylabel(r'$\\hat{\\pi}$', fontsize=fontsize)\n",
    "\n",
    "axes[1].plot(N_list, np.pi + 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2)\n",
    "axes[1].plot(N_list, np.pi - 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2, label=r'$\\pi\\pm 1/\\sqrt{N}$')\n",
    "axes[1].hlines(np.pi, 0, N_list[-1], colors='red', linestyles='dashed', linewidth=3)\n",
    "axes[1].legend(fontsize=fontsize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ultimate plot customization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def customize_plot(ax=None, **kwargs):\n",
    "    kw = dict(\n",
    "        tick_pad = 15,\n",
    "        tick_width = 2,\n",
    "        tick_length = 10,\n",
    "        fontsize_tick = 35,\n",
    "        max_n_ticks = 5,\n",
    "        labelpad = 25\n",
    "    )\n",
    "    kw.update(kwargs)\n",
    "    \n",
    "    if ax is None:\n",
    "        ax = plt.gca()\n",
    "    for tick in ax.xaxis.get_major_ticks():\n",
    "        tick.label.set_fontsize(kw['fontsize_tick'])\n",
    "        tick.set_pad(kw['tick_pad'])\n",
    "    for tick in ax.yaxis.get_major_ticks():\n",
    "        tick.label.set_fontsize(kw['fontsize_tick'])\n",
    "        tick.set_pad(kw['tick_pad'])\n",
    "        \n",
    "    ax.yaxis.set_major_locator(MaxNLocator(kw['max_n_ticks'], prune=\"lower\"))\n",
    "    \n",
    "    ax.xaxis.labelpad = kw['labelpad']\n",
    "    ax.yaxis.labelpad = kw['labelpad']\n",
    "    \n",
    "    ax.xaxis.set_tick_params(which='major', width=kw['tick_width'], length=kw['tick_length'])\n",
    "    ax.yaxis.set_tick_params(which='major', width=kw['tick_width'], length=kw['tick_length'])\n",
    "    \n",
    "    factor = 0.7\n",
    "    ax.xaxis.set_tick_params(which='minor', width=kw['tick_width']*factor, length=kw['tick_length']*factor)\n",
    "    ax.yaxis.set_tick_params(which='minor', width=kw['tick_width']*factor, length=kw['tick_length']*factor)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a single figure, bigger, with two axes this time\n",
    "figsize=(30,15)\n",
    "fig, axes = plt.subplots(1, 2, figsize=figsize)\n",
    "\n",
    "# Left plot\n",
    "marker_size = 200\n",
    "axes[0].scatter(pts_in_lp[:,0], pts_in_lp[:,1], s=marker_size)\n",
    "axes[0].scatter(pts_out_lp[:,0], pts_out_lp[:,1], marker='+', s=int(marker_size*3))\n",
    "axes[0].plot(xc, yc)\n",
    "axes[0].plot(square[:,0], square[:,1])\n",
    "axes[0].axis('off')\n",
    "axes[0].text(-0.2, -1.2, rf'$N = {N_for_plot}$', fontsize=fontsize)\n",
    "\n",
    "# Right plot\n",
    "axes[1].plot(N_list, pi_est_list, color='black')\n",
    "axes[1].set_xscale('log')\n",
    "axes[1].set_xlabel('Number of samples N', fontsize=fontsize)\n",
    "axes[1].set_ylabel(r'$\\hat{\\pi}$', fontsize=fontsize)\n",
    "\n",
    "axes[1].plot(N_list, np.pi + 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2)\n",
    "axes[1].plot(N_list, np.pi - 1/np.sqrt(N_list), color='blue', linestyle='dashed', linewidth=2, label=r'$\\pi\\pm 1/\\sqrt{N}$')\n",
    "axes[1].hlines(np.pi, 0, N_list[-1], colors='red', linestyles='dashed', linewidth=3)\n",
    "axes[1].legend(fontsize=fontsize)\n",
    "customize_plot(axes[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
