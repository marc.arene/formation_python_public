# How to set git on windows

## Git SCM
1. Download Git SCM [here](git-scm.com/download/win)
2. Execute the `.exe` and follow installation
3. Launch Git Bash terminal and set your user name and email in the global configurations
```
$ git config --global user.name "Marc Arene"
$ git config --global user.email marc.arene@gutilab.com
```


## Github for Windows
1. Download Github for Windows [here](http://windows.github.com)


## Git on Visual Studio Code
Use Visual Studio Code to manage you Git repos