# Formation Python

## Préliminaires
- Jupyter notebook ou Microsoft Azure ?
  - Slide de présentation sur jupyter notebook?
- IDE (integrated development envrionment): Visual Studio ou PyCharm?
  - https://tangenttechnologies.ca/blog/pycharm-vs-vscode/
- PEP8
- Regarder vidéos de formations, comment les autres approchent python pour les débutants
  - onglet Learning dans Anaconda Navigator
  - site du zéro
- Regarder la formation d'annecy

## Contenu en grosses lignes
- Python in a nutshell
  - Revue sommaire
    - c'est quoi comme langage info? historique etc
    - avantages
      - gratuit, grosse communauté, évolutif, simplicité et flexibilité de syntaxe, rapidité de mise en place, orienté objet et fonctionnel (?), bcp de librairies déjà existante, pas de gestion de mémoire
    - inconvénients
      - simplicité et flexibilité de syntaxe => rigueur
      - gestion des versions de librairies: conda vs pip vs brew
      - non-typage des variables => Identification des erreurs potentiellement très pernicieux
  - Scripts simples
    - Les classiques: boucle for, if else
    - Créer une fonction
    - Importer une librairie
    - Lire un fichier, écrire un fichier
    - Utiliser le `if __name__ == '__main__':`
  - Les librairies incontournables
    - capture d'écran de la vidéo getting started with anaconda: https://anaconda.cloud/tutorials/getting-started-with-anaconda-individual-edition, qui montre "Why is Data Science so complicated?"
    - sys, os
    - numpy, scipy, panda
    - json
    - matplotlib, plotly
    - scikit-learn, keras, tensorflow
    - time, datetime
    - IPython
    - Librairies que je ne connais pas mais qui pourraient intéresser:
      - scrappy
      - django, flask
      - sqlalchemy
      - pywin32
- Eviter les boucles for
  - examples avec numpy
- Conda: installer python et utiliser des environements virtuels
  - Regarder videos explicatives, https://anaconda.cloud/tutorials/getting-started-with-anaconda-individual-edition
  - Anaconda navigator vs terminal
    - add conda-forge to the available channels in anaconda navigator or from terminal
- Matplotlib
- Debugger son script
  - Avoir les bonnes pratiques: un code clair et rigoureux, un bon nommage des variables, des docs strings, des commentaires, de la modularité
  - regarder `~/projects/python/tutorials/School2018_2/School2018/debug`
  - IPython et utiliser `import IPython; IPython.embed()` et/ou `import ipd; ipd.set_trace()`
- Comment structurer les dossiers de son projet python ?
- Automatic testing
- Créer sa documentation automatiquement
- PEP8


## To do
- ajouter un petit notebook avancé sur les classes en python, dans un dossier "Pour aller plus loin" ?
- ajouter les liens vers les meilleures vidéos youtube de formation.
